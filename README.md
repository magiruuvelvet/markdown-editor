# Live (GitHub-flavored) Markdown Editor

Standalone Electron app

### How to use

1. Run `npm install` to grep all built dependencies.
2. Run `electron-packager . --asar=true` to bundle the app.
3. Profit!


### Resources

See `dist` for some useful files.


--

Feel free to take the code and copy it and modify it and use it however you like. (If you really want a licence, see [WTFPL](http://www.wtfpl.net/txt/copying/))
